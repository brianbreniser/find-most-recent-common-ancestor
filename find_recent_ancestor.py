#!/usr/bin/python3

from pprint import pprint as pp
import datetime
import random
import sys

if len(sys.argv) is 2:
        population_size = int(sys.argv[1])
else:
        print("No size argument given, use './find_recent_ancestor 1000 # for a population of 1000', using 100 as default")
        population_size = 100

ds = []


def initial_setup():
    ds.append([[i, set([])] for i in range(0, population_size)])
    ds.append([[i, set([])] for i in range(0, population_size)])


def add_new_generation():
    ds.append([[i, set([])] for i in range(0, population_size)])
    ds.pop(0)


def determine_parents_gen_1(x: list, y: list):
    random.seed(datetime.datetime)

    for i in x:
        p1 = random.randint(0, len(y)) - 1
        p2 = random.randint(0, len(y)) - 1
        y[p1][1] = y[p1][1] | i[1]
        y[p1][1] = y[p1][1] | set([i[0]])
        y[p2][1] = y[p2][1] | i[1]
        y[p2][1] = y[p2][1] | set([i[0]])


def determine_parents(x: list, y: list):
    random.seed(datetime.datetime)

    for i in x:
        p1 = random.randint(0, len(y)) - 1
        p2 = random.randint(0, len(y)) - 1
        y[p1][1] = y[p1][1] | i[1]
        y[p2][1] = y[p2][1] | i[1]


def does_gen_have_common_ancestor(x: list) -> bool:
    answer = False

    for i in x:
        line_answer = True

        for l in range(0, population_size):
            if l not in i[1]:
                line_answer = False

        # Hey, not a bug, if it's true, set parent to true
        # but if it's false, do nothing
        if line_answer is True:
            answer = True

    return answer


def main():
    print("Finding ancestor of population size: " + str(population_size))

    initial_setup()
    determine_parents_gen_1(ds[0], ds[1])

    for i in range(1, 100):
        if does_gen_have_common_ancestor(ds[1]) is True:
            print("Found it in " + str(i) + " generations!")
            sys.exit(0)

        add_new_generation()
        determine_parents(ds[0], ds[1])

main()
