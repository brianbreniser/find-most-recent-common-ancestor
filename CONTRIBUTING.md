If you find a bug, submit a pull request, I'll probably merge it.

If you find a way to speed it up, decrease memory, etc, I'll probably emphatically merge it :)

Thanks ahead of time to any contributors!
