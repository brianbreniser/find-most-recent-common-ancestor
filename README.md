Hi!

I like to watch numberphile on youtube: https://www.youtube.com/channel/UCoxcjq-8xIDTYp3uz647V5A

Occationally I find something really cool, and fairly simple, so I'll code it up.

This program goes along with this video: https://www.youtube.com/watch?v=Fm0hOex4psA

It's about finding the most recent ancestor in a population. If you have 6 people, and you randomly generate their parents, determine which generation will be the ancestor of every single person in the final population.

We assume some things, such as the population is stable (Always 6 at every generation), and wierd parents can happen (One person could virgin birth one person, ie, the random dice threw the same number twice in a row), also anyone can mate with anyone else to create a baby, we don't have sexes in the population. I could refine this further, but I only did what was in the youtube video. I might come back later.

Run with:
    ./find_recent_ancestor.py <number>

It will create a population with <number> size and run the simulation.

Bear in mind that it takes about 30 seconds and 2 gigs of ram to find the ancestor of 5,000 people on a core i7 laptop.
As well, about 90 seconds and 7 gigs for 10,000 people.
